import {readFile} from 'fs-extra';
import {join} from 'path';

// https://adventofcode.com/2019/day/1

const INPUT_FILE = join(__dirname, 'input.txt');

(async () => {
  const data: string = await readFile(INPUT_FILE, 'utf8');
  const masses = data.split('\n').map((mass) => Number(mass));

  const getFuelMass = (mass: number): number => Math.floor(mass / 3) - 2;

  const part1 = (): number =>
    masses.reduce((_sum, mass) => _sum + getFuelMass(mass), 0);

  const getRecusriveFuelMass = (mass: number): number => {
    const fuelMass = getFuelMass(mass);
    return fuelMass > 0 ? fuelMass + getRecusriveFuelMass(fuelMass) : 0;
  };

  const part2 = (): number =>
    masses.reduce((_sum, mass) => _sum + getRecusriveFuelMass(mass), 0);

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
  console.log('done');
})();
