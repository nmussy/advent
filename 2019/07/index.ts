import { join } from 'path';
import { readInput } from '../../util/file';
import { permutation } from 'generatorics';


// https://adventofcode.com/2019/day/7
const INPUT_FILE = join(__dirname, 'input.txt');

(async () => {
  const _memory = (await readInput(INPUT_FILE)).asNumbers(',');

  const runProgram = (phaseSequence: number[], loop = false): number => {
    let previousOutput = 0;
    const phaseOutputs: number[] = [];
    const phaseInputRequests: number[] = [];
    const phaseInputInstructionPointer: number[] = [];
    const phaseInputInstructionMemory: number[][] = [];

    do {
      for (const phaseIndex in phaseSequence) {
        const memory = phaseInputInstructionMemory[phaseIndex] || [..._memory];
        const phase = phaseSequence[phaseIndex];

        let outputCode = 0;

        for (let instructionPointer = phaseInputInstructionPointer[phaseIndex] || 0; instructionPointer < memory.length;) {
          const instruction = Number(
            memory[instructionPointer].toString().slice(-2),
          );
          const modes = memory[instructionPointer]
            .toString()
            .padStart(5, '0')
            .slice(0, 3)
            .split('')
            .reverse()
            .map((n) => Number(n));

          if (instruction === 99) {
            if (loop && Number(phaseIndex) === 4) {
              return phaseOutputs[phaseOutputs.length - 1];
            }

            break;
          }

          const getVal = (param: number, mode: number) =>
            mode ? param : memory[param];

          const param1 = getVal(memory[instructionPointer + 1], modes[0]);
          const param2 = getVal(memory[instructionPointer + 2], modes[1]);
          const output = memory[instructionPointer + 3];

          switch (instruction) {
            case 1: // add
              memory[output] = param1 + param2;
              instructionPointer += 4;
              break;
            case 2: // mul
              memory[output] = param1 * param2;
              instructionPointer += 4;
              break;
            case 3: // inp
              memory[memory[instructionPointer + 1]] = phaseInputRequests[phaseIndex] ? previousOutput : phase;
              phaseInputRequests[phaseIndex] = (phaseInputRequests[phaseIndex] || 0) + 1;
              instructionPointer += 2;
              break;
            case 4: // out
              outputCode = param1;
              previousOutput = outputCode;
              phaseOutputs.push(outputCode);
              instructionPointer += 2;
              break;
            case 5: // jnz
              instructionPointer = param1 ? param2 : instructionPointer + 3;
              break;
            case 6: // jzz
              instructionPointer = !param1 ? param2 : instructionPointer + 3;
              break;
            case 7: // lt
              memory[output] = param1 < param2 ? 1 : 0;
              instructionPointer += 4;
              break;
            case 8: // eq
              memory[output] = param1 === param2 ? 1 : 0;
              instructionPointer += 4;
              break;
            default:
              throw `unkown op code: ${memory[instructionPointer]}`;
          }

          if (loop && instruction === 4) {
            phaseInputInstructionPointer[phaseIndex] = instructionPointer;
            phaseInputInstructionMemory[phaseIndex] = memory;
            break;
          }
        }
      }
    } while (loop);

    return loop ? -1 : Math.max(...phaseOutputs);
  };

  const part1 = (): number => {
    const outputs: number[] = [];
    for (var phaseSequence of permutation([0, 1, 2, 3, 4])) {
      outputs.push(runProgram(phaseSequence));
    }

    return Math.max(...outputs);
  };

  const part2 = (): number => {
    const outputs: number[] = [];
    for (var phaseSequence of permutation([5, 6, 7, 8, 9])) {
      outputs.push(runProgram(phaseSequence, true));
    }

    return Math.max(...outputs);
  };

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
  console.log('done');
})();
