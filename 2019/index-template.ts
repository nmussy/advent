import {join} from 'path';
import {readInput} from '../util/file';

// https://adventofcode.com/2019/day/XXX

const INPUT_FILE = join(__dirname, 'input.txt');

(async () => {
  const data = (await readInput(INPUT_FILE)).asText();

  const part1 = () => {
    return '';
  };

  const part2 = () => {
    return '';
  };

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
  console.log('done');
})();
