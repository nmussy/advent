import {join} from 'path';
import {readInput} from '../../util/file';

// https://adventofcode.com/2019/day/2
const INPUT_FILE = join(__dirname, 'input.txt');

(async () => {
  const _memory = (await readInput(INPUT_FILE)).asNumbers(',');

  const runProgram = (noun: number, verb: number): number => {
    const memory = [..._memory];

    memory[1] = noun;
    memory[2] = verb;

    for (
      let instructionPointer = 0;
      instructionPointer < memory.length;
      instructionPointer += 4
    ) {
      const instruction = memory[instructionPointer];
      if (instruction === 99) {
        return memory[0];
      }

      const param1 = memory[instructionPointer + 1];
      const param2 = memory[instructionPointer + 2];
      const output = memory[instructionPointer + 3];

      switch (instruction) {
        case 1:
          memory[output] = memory[param1] + memory[param2];
          break;
        case 2:
          memory[output] = memory[param1] * memory[param2];
          break;
      }
    }

    return -1;
  };

  const part1 = (): number => runProgram(12, 2);

  const part2 = (): number => {
    for (let noun = 0; noun < 100; ++noun) {
      for (let verb = 0; verb < 100; ++verb) {
        if (runProgram(noun, verb) === 19690720) {
          return 100 * noun + verb;
        }
      }
    }
    return -1;
  };

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
  console.log('done');
})();
