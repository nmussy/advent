import {join} from 'path';
import {readInput} from '../../util/file';

// https://adventofcode.com/2019/day/3
const INPUT_FILE = join(__dirname, 'input.txt');

type Coordinate = [number, number];

(async () => {
  const wires = (await readInput(INPUT_FILE)).asMatrix('\n', ',');

  const getDirectionMatrix = (direction: string): Coordinate => {
    if (direction === 'U') {
      return [0, 1];
    } else if (direction === 'D') {
      return [0, -1];
    } else if (direction === 'R') {
      return [1, 0];
    } else if (direction === 'L') {
      return [-1, 0];
    }
  };

  const addCoordinates = (
    [x1, y1]: Coordinate,
    [x2, y2]: Coordinate,
  ): Coordinate => [x1 + x2, y1 + y2];

  const getDistance = (
    [x1, y1]: Coordinate,
    [x2, y2]: Coordinate = [0, 0],
  ): number => Math.abs(x1 - x2) + Math.abs(y1 - y2);

  const getMinDistance = (
    evalDistance: (position: Coordinate, wireIndexes: number[]) => number,
  ) => {
    const wirePositions: {[positon: string]: number}[] = [];
    let minDistance = Number.POSITIVE_INFINITY;

    wires.map((wire, wireIndex) => {
      wirePositions.push({});
      const currentWirePositions = wirePositions[wireIndex];
      const previousWirePositions = wirePositions.slice(0, wireIndex);

      let position: Coordinate = [0, 0];
      let movementIndex = 0;
      wire.map((coord) => {
        const directionMatrix = getDirectionMatrix(coord.slice(0, 1));

        let remainingDitance = Number(coord.slice(1));
        while (remainingDitance > 0) {
          position = addCoordinates(position, directionMatrix);
          const positionStr = position.join(',');
          for (const previousWirePosition of previousWirePositions) {
            if (!previousWirePosition.hasOwnProperty(positionStr)) {
              continue;
            }
            const previousIndex = previousWirePosition[positionStr];

            const distance = evalDistance(position, [
              previousIndex,
              movementIndex,
            ]);

            if (distance < minDistance) {
              minDistance = distance;
            }
          }

          if (wireIndex < wires.length - 1) {
            currentWirePositions[positionStr] = movementIndex;
          }
          remainingDitance--;
          movementIndex++;
        }
      });
    });

    return minDistance;
  };

  const part1 = () => getMinDistance((position) => getDistance(position));

  const part2 = () =>
    getMinDistance(
      (_, [previousWireIndex, currentWireIndex]) =>
        previousWireIndex + 1 + currentWireIndex + 1,
    );

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
  console.log('done');
})();
