import {join} from 'path';
import {readInput} from '../../util/file';

// https://adventofcode.com/2019/day/5
const INPUT_FILE = join(__dirname, 'input.txt');

(async () => {
  const _memory = (await readInput(INPUT_FILE)).asNumbers(',');

  const runProgram = (input: number): number => {
    const memory = [..._memory];
    let outputCode = 0;

    for (let instructionPointer = 0; instructionPointer < memory.length; ) {
      const instruction = Number(
        memory[instructionPointer].toString().slice(-2),
      );
      const modes = memory[instructionPointer]
        .toString()
        .padStart(5, '0')
        .slice(0, 3)
        .split('')
        .reverse()
        .map((n) => Number(n));

      if (instruction === 99) {
        console.log('halted at instruction #', instructionPointer);
        return outputCode;
      }

      const getVal = (param: number, mode: number) =>
        mode ? param : memory[param];

      const param1 = getVal(memory[instructionPointer + 1], modes[0]);
      const param2 = getVal(memory[instructionPointer + 2], modes[1]);
      const output = memory[instructionPointer + 3];

      switch (instruction) {
        case 1: // add
          memory[output] = param1 + param2;
          instructionPointer += 4;
          break;
        case 2: // mul
          memory[output] = param1 * param2;
          instructionPointer += 4;
          break;
        case 3: // inp
          memory[memory[instructionPointer + 1]] = input;
          instructionPointer += 2;
          break;
        case 4: // out
          outputCode = param1;
          console.log('output at #', instructionPointer, outputCode);
          instructionPointer += 2;
          break;
        case 5: // jnz
          instructionPointer = param1 ? param2 : instructionPointer + 3;
          break;
        case 6: // jzz
          instructionPointer = !param1 ? param2 : instructionPointer + 3;
          break;
        case 7: // lt
          memory[output] = param1 < param2 ? 1 : 0;
          instructionPointer += 4;
          break;
        case 8: // eq
          memory[output] = param1 === param2 ? 1 : 0;
          instructionPointer += 4;
          break;
        default:
          throw `unkown op code: ${memory[instructionPointer]}`;
      }
    }

    return -1;
  };

  const part1 = (): number => runProgram(1);
  const part2 = (): number => runProgram(5);

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
  console.log('done');
})();
