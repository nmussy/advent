// https://adventofcode.com/2019/day/4

(async () => {
  const passwordRange = [347312, 805915];

  const part1 = () => {
    let validPasswordCounter = 0;
    for (
      let password = passwordRange[0];
      password <= passwordRange[1];
      ++password
    ) {
      const numbers = password
        .toString()
        .split('')
        .map((nb) => Number(nb));

      let hasDuplicateNumber = false;
      let hasBrokenSequence = false;
      let previousNumber: number = numbers[0];
      for (const currentNumber of numbers.slice(1)) {
        if (previousNumber > currentNumber) {
          hasBrokenSequence = true;
          break;
        }
        if (previousNumber === currentNumber) {
          hasDuplicateNumber = true;
        }
        previousNumber = currentNumber;
      }

      if (hasDuplicateNumber && !hasBrokenSequence) ++validPasswordCounter;
    }
    return validPasswordCounter;
  };

  const part2 = () => {
    let validPasswordCounter = 0;
    for (
      let password = passwordRange[0];
      password <= passwordRange[1];
      ++password
    ) {
      const numbers = password
        .toString()
        .split('')
        .map((nb) => Number(nb));

      let hasSequenceOfTwoDuplicateNumber = false;
      let hasBrokenSequence = false;
      let previousNumber: number = numbers[0];
      let currentNumberIndex = 1;
      for (const currentNumber of numbers.slice(1)) {
        if (previousNumber > currentNumber) {
          hasBrokenSequence = true;
          break;
        }
        if (
          previousNumber === currentNumber &&
          numbers[currentNumberIndex + 1] !== currentNumber &&
          numbers[currentNumberIndex - 2] !== currentNumber
        ) {
          hasSequenceOfTwoDuplicateNumber = true;
        }
        previousNumber = currentNumber;
        ++currentNumberIndex;
      }

      if (hasSequenceOfTwoDuplicateNumber && !hasBrokenSequence)
        ++validPasswordCounter;
    }
    return validPasswordCounter;
  };

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
  console.log('done');
})();
