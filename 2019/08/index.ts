import { join } from 'path';
import { readInput } from '../../util/file';

// https://adventofcode.com/2019/day/5
const INPUT_FILE = join(__dirname, 'input.txt');

(async () => {
  const input = (await readInput(INPUT_FILE)).asNumbers('');

  const part1 = (): number => {
    const [width, height] = [25, 6];

    const layers = input.reduce<number[][]>((_layers, pixel, index) => {
      const layerIndex = Math.floor(index / (width * height));
      if (!_layers[layerIndex]) {
        _layers[layerIndex] = [];
      }

      const layer = _layers[layerIndex];
      layer.push(pixel);

      return _layers;
    }, []);

    let minZeroLayer: number[];
    let minZero = Number.POSITIVE_INFINITY;
    for (const layer of layers) {
      const zeroes = layer.filter(pixel => pixel === 0).length;
      if (zeroes < minZero) {
        minZero = zeroes;
        minZeroLayer = layer;
      }
    }

    return minZeroLayer.filter((pixel) => pixel === 1).length * minZeroLayer.filter((pixel) => pixel === 2).length;
  };

  const part2 = (): string => {
    const [width, height] = [25, 6];

    const layers = input.reduce<number[][][]>((_layers, pixel, index) => {
      const layerIndex = Math.floor(index / (width * height));
      const lineIndex = Math.floor((index - (layerIndex * (width * height))) / width);

      _layers[layerIndex] = _layers[layerIndex] || [];
      const layer = _layers[layerIndex];

      layer[lineIndex] = layer[lineIndex] || [];
      const line = layer[lineIndex];
      line.push(pixel);

      return _layers;
    }, []);


    const stackedLayer: number[][] = [];
    layers.reverse().map((layer) =>
      layer.map((line, lineIndex) =>
        line.map((pixel, pixelIndex) => {
          stackedLayer[lineIndex] = stackedLayer[lineIndex] || [];

          if (!stackedLayer[lineIndex][pixelIndex] || pixel !== 2) {
            stackedLayer[lineIndex][pixelIndex] = pixel;
          }
        })
      ));

    return '\n' + stackedLayer.map((line) =>
      line.map((pixel) => pixel === 1 ? '█' : ' ').join('')
    ).join('\n');;
  };

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
  console.log('done');
})();
