import {readFile} from 'fs-extra';
import {join} from 'path';

// https://adventofcode.com/2018/day/12

const INPUT_FILE = join(__dirname, 'input.txt');

(async () => {
  const data: string = (await readFile(INPUT_FILE, 'utf8')).replace(/\n$/, '');

  const lines = data.split('\n');
  /*const initialState: boolean[] = [
            false, false, false, false, false, false, false, false,
            ...lines[0].replace('initial state: ', '').split('')
                .map((isPlant) => isPlant === '#'),
            false, false, false, false, false, false, false, false,
        ];*/

  const padding =
    '................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................';

  const transitions = lines.slice(2).map((transition) => ({
    /*left: transition.slice(0, 2),
            current: transition[2],
            right: transition.slice(3, 5),*/
    slice: transition.slice(0, 5),
    result: transition[transition.length - 1] /* === '#'*/,
  }));

  const run = (generations = 20) => {
    let state: string = [
      padding,
      lines[0].replace('initial state: ', ''),
      padding,
    ].join('');

    for (let generation = 0; generation < generations; ++generation) {
      const newState = state.split('');

      for (const currentIndex of Array(state.length - 4)
        .fill(0)
        .map((_, i) => i + 2)) {
        const slice = state.slice(currentIndex - 2, currentIndex + 3);

        const transition = transitions.find(
          (transition) => slice === transition.slice,
        );

        newState[currentIndex] = (transition && transition.result) || '.';
      }

      state = newState.join('');
    }

    return state
      .split('')
      .reduce((sum, res, i) => sum + (res === '#' ? i - padding.length : 0), 0);
  };

  const part1 = () => run(20);
  const part2 = () => {
    const generations = 50000000000;
    // const generations = 131;
    /*const pattern = [];

            let generation = 0;
            while(generation < 150) {
                pattern.push(run(++generation));
            }

            const variation = pattern.map((res, i) => res - pattern[i - 1]);
            console.log(
                pattern.slice(100).map((e, i) => [i + 1, e]),
                variation.slice(100).map((e, i) => [i + 1, e])
            );*/

    const start = 10600,
      patternStartGen = 114;
    const increase = 80;

    const runs = generations - patternStartGen;
    return start + runs * increase;
  };

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
})();
