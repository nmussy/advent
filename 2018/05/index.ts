import {readFile} from 'fs-extra';
import {join} from 'path';

// https://adventofcode.com/2018/day/5

const INPUT_FILE = join(__dirname, 'input.txt');

(async () => {
  const data: string = (await readFile(INPUT_FILE, 'utf8')).replace(/\n$/, '');

  const part1 = () => {
    let output = data,
      prevOutput;
    do {
      prevOutput = output;

      let outputBackup;
      do {
        let found = false;
        outputBackup = output;
        output = output.replace(/(\w)\1/gi, ([l1, l2]) => {
          if (!found && l1 !== l2) {
            found = true;
            return '';
          }
          return l1 + l2;
        });

        // console.log(outputBackup.length, output.length);
      } while (outputBackup.length !== output.length);
      // console.log('/', prevOutput.length, output.length);
    } while (output.length !== prevOutput.length);

    return output.length;

    /*const reduceLength = (data: string) => {
            let output = [];
            let prevLetter = '';
            let done = false;
            for (const letter of data.split('')) {
                if (!done && letter !== prevLetter && letter.toLocaleLowerCase() === prevLetter.toLocaleLowerCase()) {
                    output.length--;
                    prevLetter = letter;
                    done = true;

                    continue;
                } else {
                    output.push(letter);
                }
                prevLetter = letter;
            }

            return output.join('');
        };

        let output = data, prevOutput;
        do {
            prevOutput = output;
            output = reduceLength(output);
        } while (prevOutput.length !== output.length);

        return output.length;*/
  };

  const part2 = async () => {
    const reduceLength = (data: string) => {
      const output = [];
      let prevLetter = '';
      let done = false;
      for (const letter of data.split('')) {
        if (
          !done &&
          letter !== prevLetter &&
          letter.toLocaleLowerCase() === prevLetter.toLocaleLowerCase()
        ) {
          output.length--;
          prevLetter = letter;
          done = true;

          continue;
        } else {
          output.push(letter);
        }
        prevLetter = letter;
      }

      return output.join('');
    };

    const reduceOutput = (data: string) => {
      let output = data,
        prevOutput;
      do {
        prevOutput = output;
        output = reduceLength(output);
      } while (prevOutput.length !== output.length);

      return output.length;
    };

    const alphabet = Array(26)
      .fill('')
      .map((_, i) => String.fromCharCode(i + 65));

    const res = await Promise.all(
      alphabet.map(async (letter) => {
        const a = data
          .replace(new RegExp(letter, 'g'), '')
          .replace(new RegExp(letter.toLocaleLowerCase(), 'g'), '');

        if (a.length === data.length) {
          return [];
        }

        console.log(letter);

        const out = [letter, reduceOutput(a)];

        console.log(out);
        return out;
      }),
    );
    console.log(JSON.stringify(res));

    // @ts-ignore
    return Math.min(...res.map(res[1] as any));
  };

  console.log('Part 1:', part1());
  // console.log('Part 2:', await part2());
})();
