import {readFile} from 'fs-extra';
import {join} from 'path';

// https://adventofcode.com/2018/day/6

const INPUT_FILE = join(__dirname, 'input.txt');

(async () => {
  const data: string = (await readFile(INPUT_FILE, 'utf8')).replace(/\n$/, '');

  const coordinates: [number, number][] = data
    .split('\n')
    .map((line) => line.split(', ').map((n) => Number(n)) as [number, number]);

  const part1 = () => {
    const getCoordinatesDistances = (gridCoordinates) => {
      const [oX, oY, maxX, maxY] = gridCoordinates;

      const grid: number[][] = [];
      coordinates.map((coordinate, i) => {
        const [x, y] = coordinate;

        grid[x + oX] = grid[x + oX] || [];
        grid[x + oX][y + oY] = i;
      });

      for (const _x of Array(maxX - oX)
        .fill('')
        .map((_, i) => i + oX)) {
        for (const _y of Array(maxY - oY)
          .fill('')
          .map((_, i) => i + oY)) {
          grid[_x] = grid[_x] || [];

          if (grid[_x][_y]) {
            continue;
          }

          const distances = coordinates.map((coordinate, i) => {
            const [x, y] = coordinate;

            return Math.abs(x - _x) + Math.abs(y - _y);
          });

          const minDistance = Math.min(...distances);
          if (distances.filter((d) => d === minDistance).length > 1) {
            grid[_x][_y] = -1;
            continue;
          }

          grid[_x][_y] = distances.indexOf(minDistance);
        }
      }

      return coordinates.map((_, i) =>
        grid.reduce(
          (sum, line) =>
            sum + (line && line.filter((cell) => cell === i).length) || 0,
          0,
        ),
      );
    };

    const gridCoordinates = [
      1,
      1,
      Math.max(...coordinates.map((c) => c[0])) + 1,
      Math.max(...coordinates.map((c) => c[1])) + 1,
    ];

    const grid1 = getCoordinatesDistances(gridCoordinates);

    gridCoordinates[0]--;
    gridCoordinates[1]--;
    gridCoordinates[2]++;
    gridCoordinates[3]++;
    const grid2 = getCoordinatesDistances(gridCoordinates);

    const didntChange = grid1.filter((c, i) => grid2[i] === c);

    return Math.max(...didntChange);
  };

  const part2 = () => {
    const lessThan = 10000;
    const [oX, oY] = Array(2).fill(Math.ceil(Math.sqrt(lessThan)));

    const gridSize = [
      Math.max(...coordinates.map((c) => c[0])) + oX,
      Math.max(...coordinates.map((c) => c[1])) + oY,
    ];

    const grid: number[][] = [];
    coordinates.map((coordinate, i) => {
      const [x, y] = coordinate;

      grid[x + oX] = grid[x + oX] || [];
      grid[x + oX][y + oY] = i;
    });

    const answerGrid: boolean[][] = [];
    for (const _x of Array(gridSize[0])
      .fill('')
      .map((_, i) => i)) {
      for (const _y of Array(gridSize[1])
        .fill('')
        .map((_, i) => i)) {
        answerGrid[_x] = answerGrid[_x] || [];

        const sumDistance = coordinates.reduce((sum, coordinate) => {
          if (sum >= lessThan) {
            return sum;
          }

          const [x, y] = coordinate;

          return sum + Math.abs(x - _x) + Math.abs(y - _y);
        }, 0);

        if (sumDistance < lessThan) {
          answerGrid[_x][_y] = true;
        }
      }
    }

    return answerGrid.reduce(
      (sum, line) => sum + (line && line.filter((cell) => cell).length) || 0,
      0,
    );
  };

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
})();
