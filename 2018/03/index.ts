import {readFile} from 'fs-extra';
import {join} from 'path';

// https://adventofcode.com/2018/day/3

const INPUT_FILE = join(__dirname, 'fabric.txt');

(async () => {
  const data: string = await readFile(INPUT_FILE, 'utf8');

  type NumberTuple = [number, number];
  type Fabric = {id: number; coord: NumberTuple; size: NumberTuple};

  const fabrics: Fabric[] = data
    .split('\n')
    .filter((str) => !!str)
    .map((fabric) => {
      const [id, _, coord, size] = fabric.split(' ');

      return {
        id: Number(id.replace('#', '')),
        coord: coord
          .replace(':', '')
          .split(',')
          .map((n) => Number(n)) as [number, number],
        size: size
          .split('x')
          .map((n) => Number(n))
          .map((n) => Number(n)) as [number, number],
      };
    });

  const fillMatrix = (matrix: number[][], fabric: Fabric) => {
    const [left, top] = fabric.coord;
    const [width, height] = fabric.size;

    Array(width)
      .fill(1)
      .map((_, row) =>
        Array(height)
          .fill(1)
          .map((_1, column) => {
            matrix[left + row] = matrix[left + row] || [];
            matrix[left + row][top + column] =
              (matrix[left + row][top + column] || 0) + 1;
          }),
      );
  };

  const part1 = () => {
    const matrix: number[][] = [];

    fabrics.map((fabric) => fillMatrix(matrix, fabric));

    return matrix.reduce(
      (count, lines) =>
        count + lines.reduce((_count, col) => _count + (col > 1 ? 1 : 0), 0),
      0,
    );
  };

  const part2 = () => {
    const uniqueFabric = fabrics.find((f1) => {
      let collision = false;
      const matrix: number[][] = [];

      fillMatrix(matrix, f1);

      Object.values(fabrics).map((f2) => {
        if (f1 === f2 || collision) {
          return;
        }

        const [left, top] = f2.coord;
        const [width, height] = f2.size;

        Array(width)
          .fill(1)
          .map((_, row) =>
            Array(height)
              .fill(1)
              .map((_1, column) => {
                if (
                  collision ||
                  (matrix[left + row] && matrix[left + row][top + column])
                ) {
                  collision = true;
                }
              }),
          );
      });

      return !collision;
    });

    return uniqueFabric.id;
  };

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
})();
