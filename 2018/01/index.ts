import {readFile} from 'fs-extra';
import {join} from 'path';

// https://adventofcode.com/2018/day/1

const INPUT_FILE = join(__dirname, 'frequencies.txt');

(async () => {
  const data: string = await readFile(INPUT_FILE, 'utf8');

  const frequencies: number[] = data
    .split('\n')
    .map((frequency) => Number(frequency));

  const part1 = (): number =>
    frequencies.reduce((res, frequency) => res + frequency, 0);

  const part2 = (): number => {
    const previousFrequencies: number[] = [];

    let lastFrequency = 0;
    while (true) {
      for (const frequency of frequencies) {
        const nextFrequency = lastFrequency + frequency;

        if (previousFrequencies.includes(nextFrequency)) {
          return nextFrequency;
        }

        previousFrequencies.push((lastFrequency = nextFrequency));
      }
    }
  };

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
})();
