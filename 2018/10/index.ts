import {readFile, writeFile} from 'fs-extra';
import {join} from 'path';

// https://adventofcode.com/2018/day/10

const INPUT_FILE = join(__dirname, 'input.txt');

(async () => {
  const data: string = (await readFile(INPUT_FILE, 'utf8')).replace(/\n$/, '');

  const vectors = data.split('\n').map((line) => ({
    position: line
      .match(/position=<\s*(-?\d+),\s*(-?\d+)>/)
      .slice(1)
      .map(Number),
    velocity: line
      .match(/velocity=<\s*(-?\d+),\s*(-?\d+)>/)
      .slice(1)
      .map(Number),
  }));

  // console.log(vectors);

  const part1 = async () => {
    // const matrix: string[][] = [];

    let minAmplitude = null;

    let rounds = 0;
    for (const i in Array(100000)
      .fill(0)
      .map((_, i) => i)) {
      let minX = Number.MAX_SAFE_INTEGER;
      let minY = Number.MAX_SAFE_INTEGER;

      let maxX = 0;
      let maxY = 0;

      vectors.map((vector) => {
        if (vector.position[0] < minX) {
          minX = vector.position[0];
        }
        if (vector.position[0] > maxX) {
          maxX = vector.position[0];
        }
        if (vector.position[1] < minY) {
          minY = vector.position[1];
        }
        if (vector.position[1] > maxY) {
          maxY = vector.position[1];
        }

        vector.position[0] += vector.velocity[0];
        vector.position[1] += vector.velocity[1];
      }, []);

      const amplitude = maxX - minX + (maxY + minY);

      ++rounds;
      if (minAmplitude !== null && amplitude > minAmplitude) {
        break;
      }

      minAmplitude = amplitude;
    }

    console.log(rounds);

    await Promise.all(
      Array(20)
        .fill(0)
        .map((_, index) => index - 10)
        .map(async (index) => {
          const matrix = vectors.reduce((_matrix, vector) => {
            // @ts-ignore
            const x = vector.position[0] + vector.velocity[0] * index;
            // @ts-ignore
            const y = vector.position[1] + vector.velocity[1] * index;

            _matrix[x] = _matrix[x] || Array(200).fill(' ');
            _matrix[x][y] = '#';

            return _matrix;
          }, []);

          await writeFile(
            join(__dirname, index + '.txt'),
            matrix /*.filter(line => !!line)*/
              .map((line) => line.join(''))
              .join('\n'),
          );
        }),
    );
  };

  console.log('Part 1:', part1());
  // console.log('Part 2:', part2());
})();
