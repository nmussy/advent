import {readFile} from 'fs-extra';
import {join} from 'path';
import moment from 'moment';

// https://adventofcode.com/2018/day/4

const INPUT_FILE = join(__dirname, 'logs.txt');

(async () => {
  const data: string = await readFile(INPUT_FILE, 'utf8');

  enum ACTIONS {
    BEGIN = 'begins shift',
    FALLS_ASLEEP = 'falls asleep',
    WAKES_UP = 'wakes up',
  }

  interface LogAction {
    guard: number;
    action: ACTIONS;
    date: moment.Moment;
    duration?: number;
  }

  let guard: number, lastAction: LogAction;
  const logs: LogAction[] = data
    .split('\n')
    .filter((log) => !!log)
    .sort()
    .map((log) => {
      const [_date, _action] = log.split(']');
      const date = moment(_date.replace('[', ''));

      const action = Object.values(ACTIONS).find(
        (action) => !!_action.match(action),
      );

      if (action === ACTIONS.BEGIN) {
        const [_, _guard] = _action.match(/Guard #(\d+)/);
        guard = Number(_guard);
        lastAction = undefined;
      }

      const duration =
        (lastAction &&
          moment.duration(date.diff(lastAction.date)).asMinutes()) ||
        undefined;

      return (lastAction = {guard, date, action, duration});
    });

  const sleepLogs = logs.filter(
    (log) => log.action === ACTIONS.WAKES_UP && log.duration,
  );

  const part1 = () => {
    const guardsSleepMinutes = sleepLogs.reduce(
      (_guards, log) =>
        (_guards[log.guard] = (_guards[log.guard] || 0) + log.duration) &&
        _guards,
      {},
    );
    const sleepMinutes = Object.values(guardsSleepMinutes) as number[];

    const maxGuardSleepDuration = Math.max(...sleepMinutes);
    const maxGuardIndex = sleepMinutes.indexOf(maxGuardSleepDuration);

    const maxGuard = Number(Object.keys(guardsSleepMinutes)[maxGuardIndex]);

    const maxGuardLogs = sleepLogs.filter((log) => log.guard === maxGuard);

    const maxMinutes: number[] = maxGuardLogs.reduce((_minutes, log) => {
      const [start, end] = [
        moment(log.date).add(-log.duration, 'minutes'),
        moment(log.date),
      ];

      const iterator = moment(start);
      while (iterator < end) {
        _minutes[iterator.minute()] = (_minutes[iterator.minute()] || 0) + 1;
        iterator.add(1, 'minute');
      }

      return _minutes;
    }, []);

    const maxMinute = maxMinutes.indexOf(Math.max(...maxMinutes));

    return maxGuard * maxMinute;
  };

  const part2 = () => {
    const guardsMaxMinutes = sleepLogs.reduce((_guards, log) => {
      const end = moment(log.date);
      const start = moment(log.date).add(-log.duration, 'minutes');

      _guards[log.guard] = _guards[log.guard] || [];

      const iterator = moment(start);
      while (iterator < end) {
        _guards[log.guard][iterator.minute()] =
          (_guards[log.guard][iterator.minute()] || 0) + 1;
        iterator.add(1, 'minute');
      }

      return _guards;
    }, {});

    const guardsMaxMinute = Object.keys(guardsMaxMinutes).reduce(
      (_guards, guard) => {
        _guards[guard] = Math.max(...guardsMaxMinutes[guard].filter((n) => n));
        return _guards;
      },
      {},
    );

    const maxMinuteValue = Math.max(
      ...(Object.values(guardsMaxMinute) as number[]),
    );
    const maxMinuteIndex = Object.values(guardsMaxMinute).indexOf(
      maxMinuteValue,
    );
    const maxMinuteGuard = Number(Object.keys(guardsMaxMinute)[maxMinuteIndex]);

    const maxMinute = guardsMaxMinutes[maxMinuteGuard].indexOf(maxMinuteValue);

    return maxMinute * maxMinuteGuard;
  };

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
})();
