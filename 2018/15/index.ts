import {readFile} from 'fs-extra';
import {join} from 'path';

// @ts-ignore
import PF from 'pathfinding';

// https://adventofcode.com/2018/day/15

//const INPUT_FILE = join(__dirname, 'input.txt');

// 37 * 982 = 36334
const INPUT_FILE = join(__dirname, 'c1.txt');

// 46 * 859 = 39514
// const INPUT_FILE = join(__dirname, 'c2.txt');

// 35 * 793 = 27755
// const INPUT_FILE = join(__dirname, 'c3.txt');

// const INPUT_FILE = join(__dirname, 'combatexample.txt');

//const INPUT_FILE = join(__dirname, 'moveexample.txt');

enum ENTITY {
  WALL = '#',
  OPEN = '.',
  GOBLIN = 'G',
  ELF = 'E',
  DEAD = 'X',
}

interface Unit {
  entity: ENTITY;
  position: [number, number];
  alive: boolean;
  attack: number;
  hp: number;
}

(async () => {
  const data: string = (await readFile(INPUT_FILE, 'utf8')).replace(/\n$/, '');

  const state: ENTITY[][] = data
    .split('\n')
    .map((line) => line.split('').map((entity) => entity as ENTITY));

  const units: Unit[] = [].concat(
    ...state.map((line, x) =>
      line
        .map(
          (entity, y): Unit => ({
            entity,
            position: [x, y],
            alive: true,
            attack: 3,
            hp: 200,
          }),
        )
        .filter((entity) =>
          [ENTITY.ELF, ENTITY.GOBLIN].includes(entity.entity),
        ),
    ),
  );

  type DistanceType = {
    distance: number;
    position: [number, number];
    targetTile: [number, number];
  };

  const adjacents = [
    [0, -1],
    [0, 1],
    [-1, 0],
    [1, 0],
  ];

  const finder = new PF.AStarFinder({
    allowDiagonal: false,
    dontCrossCorners: true,
    heuristic: (dx, dy) => (dx >= dy ? dx : dy),
    //heuistic: PF.Heuristic.octile
  });
  const getDistance = (
    unit: [number, number],
    target: [number, number],
    grid: any,
  ): DistanceType => {
    const [unitX, unitY] = unit;
    const [targetX, targetY] = target;

    // FIXME consider wall for distance?
    const adjacentsTarget: DistanceType[] = adjacents
      .map(
        (adjacent) =>
          [targetX + adjacent[0], targetY + adjacent[1]] as [number, number],
      )
      .filter(
        (tile) => state[tile[0]] && state[tile[0]][tile[1]] === ENTITY.OPEN,
      )
      .map((position) => {
        const path = finder.findPath(
          unitX,
          unitY,
          position[0],
          position[1],
          grid.clone(),
        );
        return {
          position,
          targetTile: path[1],
          distance: path.length,
        };
      })
      .filter((d) => !!d.distance);

    if (!adjacentsTarget) {
      return;
    }

    return adjacentsTarget.sort((t1, t2) =>
      t1.distance === t2.distance
        ? t1.position[0] === t2.position[0]
          ? t1.position[1] > t2.position[1]
            ? 1
            : -1
          : t1.position[0] > t2.position[0]
          ? 1
          : -1
        : t1.distance > t2.distance
        ? 1
        : -1,
    )[0];
    // return {distance, position};
  };

  const getDirection = (
    unit: [number, number],
    targetTile: [number, number],
  ): [number, number] => {
    const [unitX, unitY] = unit;
    const [targetX, targetY] = targetTile;

    const possibleMoves = [
      [
        unitX !== targetX ? (unitX > targetX ? unitX - 1 : unitX + 1) : unitX,
        unitY,
      ],
      [
        unitX,
        unitY !== targetY ? (unitY > targetY ? unitY - 1 : unitY + 1) : unitY,
      ],
    ].filter((move) => move[0] !== unitX || move[1] !== unitY) as [
      number,
      number,
    ][];

    const move = possibleMoves.sort((u1, u2) =>
      u1[0] === u2[0] ? (u1[1] > u2[1] ? 1 : -1) : u1[0] > u2[0] ? 1 : -1,
    )[0];

    if (state[move[0]][move[1]] !== ENTITY.OPEN) {
      console.error('WALL');
    }

    return move;
  };

  const move = (unit: Unit, position: [number, number]) => {
    state[unit.position[0]][unit.position[1]] = ENTITY.OPEN;
    unit.position = position;
    state[unit.position[0]][unit.position[1]] = unit.entity;
  };

  const getAlive: () => Unit[] = () => units.filter((unit) => unit.alive);

  let turn = 0;
  const run = () => {
    let turnFinished = true;

    getAlive()
      .sort((u1, u2) =>
        u1.position[0] === u2.position[0]
          ? u1.position[1] > u2.position[1]
            ? 1
            : -1
          : u1.position[0] > u2.position[0]
          ? 1
          : -1,
      )
      .map((unit: Unit, index) => {
        if (!unit.alive) {
          return;
        }

        if ([...new Set(getAlive().map((unit) => unit.entity))].length < 2) {
          turnFinished = false;
          return;
        }

        const enemyEntity =
          unit.entity === ENTITY.GOBLIN ? ENTITY.ELF : ENTITY.GOBLIN;

        {
          // move
          const adjascentTiles: [number, number][] = adjacents.map(
            (adjacent) =>
              [
                unit.position[0] + adjacent[0],
                unit.position[1] + adjacent[1],
              ] as [number, number],
          );

          const adjascentEntities = adjascentTiles.map(
            (tile) => state[tile[0]][tile[1]],
          );

          if (!adjascentEntities.includes(enemyEntity)) {
            const grid = new PF.Grid(state.length, state[0].length);

            state.map((line, x) =>
              line.map((entity, y) => {
                grid.setWalkableAt(
                  x,
                  y,
                  entity === ENTITY.OPEN ||
                    (x === unit.position[0] && y === unit.position[1]),
                );
                // FIXME weigh only adjacent tiles?
                grid.setWeightAt(x, y, x * state.length + y);
              }),
            );

            const enemy = getAlive()
              .filter((unit) => unit.entity === enemyEntity)
              .map((enemy) => ({
                enemy,
                distance: getDistance(
                  unit.position,
                  enemy.position,
                  grid.clone(),
                ),
              }))
              .filter((enemy) => !!enemy.distance)
              .sort((e1, e2) =>
                e1.distance.distance === e2.distance.distance
                  ? e1.distance.position[0] === e2.distance.position[0]
                    ? e1.distance.position[1] > e2.distance.position[1]
                      ? 1
                      : -1
                    : e1.distance.position[0] > e2.distance.position[0]
                    ? 1
                    : -1
                  : e1.distance.distance > e2.distance.distance
                  ? 1
                  : -1,
              )[0];

            if (!enemy) {
              return;
            }

            const direction = getDirection(
              unit.position,
              enemy.distance.targetTile,
            );

            if (direction) {
              move(unit, direction);
            } else {
              // console.error('no direction');
            }
          }
        }

        {
          // attack
          const adjascentTiles: [number, number][] = adjacents.map(
            (adjacent) =>
              [
                unit.position[0] + adjacent[0],
                unit.position[1] + adjacent[1],
              ] as [number, number],
          );

          const adjascentEntities = adjascentTiles.filter(
            (tile) => state[tile[0]][tile[1]] === enemyEntity,
          );
          const enemy: Unit = adjascentEntities
            .map((position) =>
              units.find(
                (unit) =>
                  unit.position[0] === position[0] &&
                  unit.position[1] === position[1],
              ),
            )
            .sort((u1, u2) =>
              u1.hp === u2.hp
                ? u1.position[0] === u2.position[0]
                  ? u1.position[1] > u2.position[1]
                    ? 1
                    : -1
                  : u1.position[0] > u2.position[0]
                  ? 1
                  : -1
                : u1.hp > u2.hp
                ? 1
                : -1,
            )[0];
          if (enemy) {
            enemy.hp -= unit.attack;

            if (enemy.hp < 1) {
              enemy.hp = 0;
              enemy.alive = false;
              console.log('DIES', enemy.entity, 'TURN', turn);
              state[enemy.position[0]][enemy.position[1]] = ENTITY.DEAD;
              console.log(state.map((line) => line.join('')).join('\n'));
              state[enemy.position[0]][enemy.position[1]] = ENTITY.OPEN;
            }
          }
        }
      });

    if (turnFinished) {
      ++turn;
    }
  };

  const part1 = () => {
    console.log(state.map((line) => line.join('')).join('\n'));

    while ([...new Set(getAlive().map((unit) => unit.entity))].length > 1) {
      run();
      console.log('TURN', turn);
      console.log(state.map((line) => line.join('')).join('\n'));
      /*
                                if (turn > 24 && turn < 28) {
                                    console.log('TURN', turn);
                                    console.log(state.map(line => line.join('')).join('\n'));
                                }
                */
    }
    console.log(state.map((line) => line.join('')).join('\n'));

    console.log(
      turn,
      units.reduce((_sum, unit) => _sum + unit.hp, 0),
    );
    return turn * units.reduce((_sum, unit) => _sum + unit.hp, 0);
  };

  const part2 = () => {};

  console.log('Part 1:', part1());
  // console.log('Part 2:', part2());
})();
