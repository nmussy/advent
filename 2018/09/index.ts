import {readFile} from 'fs-extra';
import {join} from 'path';

// https://adventofcode.com/2018/day/9

const INPUT_FILE = join(__dirname, 'input.txt');

interface Marble {
  val: number;
  next?: Marble;
  prev?: Marble;
}
// FIXME finish refactor
(async () => {
  const data: string = (await readFile(INPUT_FILE, 'utf8')).replace(/\n$/, '');

  /*const [_, players, lastVal] = data
            .match(/(\d+) players; last marble is worth (\d+) points/)
            .map(n => Number(n));*/

  const [players, lastVal] = [9, 25];
  const back = 7;

  const getInit = (): Marble => {
    const marble: Marble = {val: 0};
    marble.next = marble.prev = marble;

    return marble;
  };

  const round = (lastMarble: Marble, scores: number[]): Marble => {
    const nextVal = lastMarble.val + 1;

    if (nextVal % 23 === 0) {
      const player = nextVal % players;

      let marble = lastMarble;
      for (const _ of Array(back).fill(0)) {
        marble = marble.prev;
      }

      scores[player] = (scores[player] || 0) + nextVal + marble.prev.val;

      // remove middle from list
      marble.prev.prev.next = marble;
      marble.prev = marble.prev.prev;

      return marble;
    }

    const target = lastMarble.next;

    const nextMarble = {
      val: nextVal,
      prev: target,
      next: target.next,
    };

    console.log(target.next.val, target.next.prev.val);

    target.next = /*target.next.prev = */ nextMarble;

    return nextMarble;
  };

  const run = (lastVal): number[] => {
    let lastMarble = getInit();

    const scores: number[] = [];
    while (lastMarble.val < lastVal) {
      lastMarble = round(lastMarble, scores);
    }

    return scores;
  };

  // 409832
  const part1 = () => {
    const scores = run(lastVal);
    Math.max(...scores.filter((score) => !!score));
  };

  // 3469562780
  const part2 = () => {
    const scores = run(lastVal * 100);
    Math.max(...scores.filter((score) => !!score));
  };

  console.log('Part 1:', part1());
  // console.log('Part 2:', part2());
})();
