import {readFile} from 'fs-extra';
import {join} from 'path';

// https://adventofcode.com/2018/day/8

const INPUT_FILE = join(__dirname, 'input.txt');

interface Node {
  sum: number;
  value: number;
  deepSum: number;
  children: Node[];
}

(async () => {
  const data: string = (await readFile(INPUT_FILE, 'utf8')).replace(/\n$/, '');

  const numbers: number[] = data.split(' ').map((n) => Number(n));

  let iterator = 0;
  const parseNodes = (expectedChildren = Number.MAX_SAFE_INTEGER): Node[] => {
    const nodes: Node[] = [];
    while (nodes.length < expectedChildren && iterator < numbers.length) {
      const childrenLength = numbers[iterator++] || 0;
      const metaLength = numbers[iterator++] || 0;

      const children = parseNodes(childrenLength);
      const meta = numbers.slice(iterator, (iterator += metaLength));
      const sum = meta.reduce((_sum, meta) => _sum + meta, 0);
      const deepSum =
        sum + children.reduce((_sum, child) => _sum + child.deepSum, 0);

      const value = childrenLength
        ? meta.reduce(
            (sum, index) =>
              sum + ((children[index - 1] && children[index - 1].value) || 0),
            0,
          )
        : sum;

      nodes.push({children, sum, value, deepSum});
    }

    return nodes;
  };

  const [root] = parseNodes();

  const part1 = () => root.deepSum;

  const part2 = () => root.value;

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
})();
