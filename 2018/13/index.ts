import {readFile} from 'fs-extra';
import {join} from 'path';

// https://adventofcode.com/2018/day/13

const INPUT_FILE = join(__dirname, 'input.txt');

enum INTERSECTION_OPERATION {
  LEFT = 'LEFT',
  STRAIGHT = 'STRAIGHT',
  RIGHT = 'RIGHT',
}

enum DIRECTION {
  UP = '^',
  RIGHT = '>',
  DOWN = 'v',
  LEFT = '<',
}

enum TRACK {
  INTERSECTION = '+',
  HORIZONTAL = '-',
  VERTICAL = '|',
  POSITIVE_ANGLE = '/',
  NEGATIVE_ANGLE = '\\',
}

interface Cart {
  position: [number, number];
  nextOperation: INTERSECTION_OPERATION;
  direction: DIRECTION;
  crashed: boolean;
}

(async () => {
  const data: string = (await readFile(INPUT_FILE, 'utf8')).replace(/\n$/, '');

  const operations = [
    INTERSECTION_OPERATION.LEFT,
    INTERSECTION_OPERATION.STRAIGHT,
    INTERSECTION_OPERATION.RIGHT,
  ];

  const [firstOperation] = operations;

  const directionsValues = Object.values(DIRECTION) as DIRECTION[];

  const [firstDirection] = directionsValues;
  const [lastDirection] = [...directionsValues].reverse();

  const tracks: TRACK[][] = data.split('\n').map(
    (line) =>
      line
        .replace(/\^|v/g, TRACK.HORIZONTAL)
        .replace(/<|>/g, TRACK.VERTICAL)
        .split('') as TRACK[],
  );

  const carts: Cart[] = [].concat(
    ...data
      .split('\n')
      .map((line) => line.split(''))
      .map((line, x) =>
        line
          .map((direction: DIRECTION, y) => ({direction, y}))
          .filter(({direction}: {direction: DIRECTION}) =>
            directionsValues.includes(direction),
          )
          .map(
            ({direction, y}) =>
              ({
                nextOperation: firstOperation,
                direction,
                position: [x, y],
              } as Cart),
          ),
      ),
  );

  const run = (): Cart[] => {
    const collisions: Cart[] = [];

    carts
      .filter((cart) => !cart.crashed)
      .sort((c1, c2) =>
        c1.position[0] !== c2.position[0]
          ? c1.position[0] > c2.position[0]
            ? 1
            : -1
          : c1.position[1] > c2.position[1]
          ? 1
          : -1,
      )
      .map((cart) => {
        // in case the cart was crashed into this current tick
        if (cart.crashed) {
          return cart;
        }

        const track = tracks[cart.position[0]][cart.position[1]];

        if ([TRACK.POSITIVE_ANGLE, TRACK.NEGATIVE_ANGLE].includes(track)) {
          if ([DIRECTION.DOWN, DIRECTION.UP].includes(cart.direction)) {
            if (
              (cart.direction === DIRECTION.DOWN &&
                track === TRACK.NEGATIVE_ANGLE) ||
              (cart.direction === DIRECTION.UP &&
                track === TRACK.POSITIVE_ANGLE)
            ) {
              cart.direction = DIRECTION.RIGHT;
            } else {
              cart.direction = DIRECTION.LEFT;
            }
          } else {
            if (
              (cart.direction === DIRECTION.LEFT &&
                track === TRACK.NEGATIVE_ANGLE) ||
              (cart.direction === DIRECTION.RIGHT &&
                track === TRACK.POSITIVE_ANGLE)
            ) {
              cart.direction = DIRECTION.UP;
            } else {
              cart.direction = DIRECTION.DOWN;
            }
          }
        } else if (track === TRACK.INTERSECTION) {
          switch (cart.nextOperation) {
            case INTERSECTION_OPERATION.STRAIGHT:
              break;
            case INTERSECTION_OPERATION.RIGHT:
              cart.direction =
                directionsValues[
                  directionsValues.indexOf(cart.direction) + 1
                ] || firstDirection;
              break;
            case INTERSECTION_OPERATION.LEFT:
              cart.direction =
                directionsValues[
                  directionsValues.indexOf(cart.direction) - 1
                ] || lastDirection;
              break;
          }

          cart.nextOperation =
            operations[operations.indexOf(cart.nextOperation) + 1] ||
            firstOperation;
        }

        const increment: [number, number] = [0, 0];
        switch (cart.direction) {
          case DIRECTION.UP:
            increment[0]--;
            break;
          case DIRECTION.DOWN:
            increment[0]++;
            break;
          case DIRECTION.LEFT:
            increment[1]--;
            break;
          case DIRECTION.RIGHT:
            increment[1]++;
            break;
        }

        cart.position[0] += increment[0];
        cart.position[1] += increment[1];

        const collidedCart = carts
          .filter((cart) => !cart.crashed)
          .find(
            (c) =>
              c !== cart &&
              c.position[0] === cart.position[0] &&
              c.position[1] === cart.position[1],
          );

        if (collidedCart) {
          cart.crashed = true;
          collidedCart.crashed = true;

          collisions.push(...[cart, collidedCart]);
        }

        return cart;
      });

    return collisions;
  };

  const part1 = () => {
    let collidedCarts: Cart[];
    while (!(collidedCarts = run()).length) {}

    const [firstCollidedCart] = collidedCarts;
    return firstCollidedCart.position.reverse().join(',');
  };

  const part2 = () => {
    while (run()) {
      const remainingCarts = carts.filter((cart) => !cart.crashed);

      if (remainingCarts.length === 1) {
        const [remainingCart] = remainingCarts;
        return remainingCart.position.reverse().join(',');
      }
    }
  };

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
})();
