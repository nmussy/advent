import {readFile} from 'fs-extra';
import {join} from 'path';
import {get} from 'fast-levenshtein';

// https://adventofcode.com/2018/day/2

const INPUT_FILE = join(__dirname, 'checksum.txt');

(async () => {
  const data: string = await readFile(INPUT_FILE, 'utf8');

  const checksums = data.split('\n');

  const part1 = () => {
    const occurrences: number[] = [];

    checksums.map((checksum) => {
      const letterSums = checksum
        .split('')
        .reduce(
          (letterSum, letter) =>
            (letterSum[letter] = (letterSum[letter] || 0) + 1) && letterSum,
          {} as {[letter: string]: number},
        );

      [...new Set(Object.values(letterSums).filter((sum) => sum >= 2))].map(
        (values) => (occurrences[values] = (occurrences[values] || 0) + 1),
      );
    });

    return occurrences.reduce((product, occurrence) => product * occurrence, 1);
  };

  const part2 = () => {
    const [[c1], [c2]] = checksums
      .map((c1, i1) =>
        checksums.filter((c2, i2) => i2 !== i1 && get(c1, c2) === 1),
      )
      .filter((res) => !!res.length);

    return c1
      .split('')
      .filter((char, index) => c2[index] === char)
      .join('');
  };

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
})();
