// https://adventofcode.com/2018/day/11

console.time('2');
const gridSerial = 18;

const powerLevel = (x: number, y: number) => {
  const rackId = x + 10;
  let power = (rackId * y + gridSerial) * rackId;
  power = Number((power + '').split('').reverse()[2]) || 0;

  return power - 5;
};

console.time('powers');
const powers = ((): number[][] => {
  const powers: number[][] = [];
  for (const __x in Array(300 - 2).fill(0)) {
    const x = Number(__x) + 1;
    powers[x] = [];
    for (const __y in Array(300 - 2).fill(0)) {
      const y = Number(__y) + 1;
      powers[x][y] = powerLevel(x, y);
    }
  }

  return powers;
})();
console.timeEnd('powers');

const part2 = () => {
  const maxSquare = 300 - 2;

  const _coordinates = Array(maxSquare)
    .fill(0)
    .map((_, i) => i + 1);

  let max = 0,
    coord = [],
    size = 0;
  for (const x of _coordinates) {
    for (const y of _coordinates) {
      let power = 0;

      const _sizes = Array(maxSquare - Math.max(x, y))
        .fill(0)
        .map((_, i) => i);

      for (const curSize of _sizes) {
        power +=
          powers[x + curSize][y + curSize] +
          Array(curSize)
            .fill(0)
            .reduce(
              (_sum, _, i) =>
                _sum + powers[x + i][curSize + y] + powers[curSize + x][y + i],
              0,
            );

        if (power > max) {
          max = power;
          coord = [x, y];
          size = curSize + 1;
        }
      }
    }
  }

  return `${coord.toString()},${size}`;
};

console.log('Part 2:', part2());
console.timeEnd('2');
