// https://adventofcode.com/2018/day/14

(async () => {
  const input = 760221;

  const data = [3, 7];
  const currentRecipesIndex = [0, 1];

  const run = () => {
    const currentRecipes = currentRecipesIndex.map((index) => data[index]);
    const newRecipe = currentRecipes[0] + currentRecipes[1];

    data.push(...(newRecipe + '').split('').map(Number));

    currentRecipesIndex[0] =
      (currentRecipesIndex[0] + currentRecipes[0] + 1) % data.length;
    currentRecipesIndex[1] =
      (currentRecipesIndex[1] + currentRecipes[1] + 1) % data.length;
  };

  const part1 = () => {
    while (data.length < input + 10) {
      run();
      // console.log(data);
    }

    return data.slice(input, input + 10).join('');
  };

  const part2 = () => {
    const _input = input + '';
    let index;
    let previousLength = 0;
    while (true) {
      run();
      if (
        (index = data
          .slice(previousLength - _input.length)
          .join('')
          .indexOf(_input)) !== -1
      ) {
        return index + (previousLength - _input.length);
      }

      previousLength = data.length;
    }
  };

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
})();
