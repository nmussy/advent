import {readFile} from 'fs-extra';
import {join} from 'path';

// https://adventofcode.com/2018/day/7

const INPUT_FILE = join(__dirname, 'input.txt');

(async () => {
  const data: string = (await readFile(INPUT_FILE, 'utf8')).replace(/\n$/, '');

  const requirements = data
    .split('\n')
    .map((requirement) => {
      const match = requirement.match(
        /Step (\w+) must be finished before step (\w+)/,
      );

      return [match[1], match[2]];
    })
    .sort((a, b) => a.join('').localeCompare(b.join('')));

  const letters: string[] = [
    ...new Set([
      ...requirements.map((r) => r[0]),
      ...requirements.map((r) => r[1]),
    ]),
  ].sort((a, b) => a.localeCompare(b));

  const part1 = () => {
    const calc = (satisfied = '') =>
      satisfied +
      letters
        .filter((letter) => !satisfied.includes(letter))
        .find(
          (letter) =>
            !requirements
              .filter((requirement) => requirement[1] === letter)
              .find((requirement) => !satisfied.includes(requirement[0])),
        );

    let satisfied = '';
    while (satisfied.length < letters.length) {
      satisfied = calc(satisfied);
    }

    return satisfied;
  };

  const part2 = () => {
    const calc = (
      satisfied = '',
      running = '',
    ): {letter: string; remaining: number}[] => {
      const availableWorkers = 5 - running.length;
      if (!availableWorkers) {
        return [];
      }

      const res = letters
        .filter(
          (letter) => !running.includes(letter) && !satisfied.includes(letter),
        )
        .filter(
          (letter) =>
            !requirements
              .filter((requirement) => requirement[1] === letter)
              .find((requirement) => !satisfied.includes(requirement[0])),
        )
        .slice(0, availableWorkers);

      return res.map((letter) => ({
        letter,
        remaining: 60 + (letter.charCodeAt(0) - 64),
      }));
    };

    let satisfied = '';
    let time = 0;

    let running = [];
    while (satisfied.length < letters.length) {
      running.push(...calc(satisfied, running.map((r) => r.letter).join('')));
      ++time;

      running.map((running) => --running.remaining);
      {
        const done = running.filter((running) => !running.remaining);
        satisfied += done.map((res) => res.letter).join('');
      }

      running = running.filter((running) => running.remaining);
    }

    return time;
  };

  console.log('Part 1:', part1());
  console.log('Part 2:', part2());
})();
