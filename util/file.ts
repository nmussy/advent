import {readFile} from 'fs-extra';

export interface ReadInput {
  asPlain: () => string;
  asText: (separator?: string) => string[];
  asNumbers: (separator?: string) => number[];
  asMatrix: (rowSeparator?: string, columnSeparator?: string) => string[][];
  asNumberMatrix: (
    rowSeparator?: string,
    columnSeparator?: string,
  ) => number[][];
}

export const readInput = async (path: string): Promise<ReadInput> => {
  const content = (await readFile(path, 'utf8')).trim();

  return {
    asPlain: () => content,
    asText: (separator = '\n') => content.split(separator),
    asNumbers: (separator = '\n') =>
      content.split(separator).map((nb) => Number(nb)),
    asMatrix: (rowSeparator = '\n', columnSeparator = ' ') =>
      content.split(rowSeparator).map((line) => line.split(columnSeparator)),
    asNumberMatrix: (rowSeparator = '\n', columnSeparator = ' ') =>
      content
        .split(rowSeparator)
        .map((line) => line.split(columnSeparator).map((nb) => Number(nb))),
  };
};
